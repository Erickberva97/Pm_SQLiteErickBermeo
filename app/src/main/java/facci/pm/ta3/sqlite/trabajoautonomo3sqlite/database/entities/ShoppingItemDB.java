package facci.pm.ta3.sqlite.trabajoautonomo3sqlite.database.entities;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.provider.BaseColumns;
import android.provider.ContactsContract;
import android.support.v7.app.AppCompatActivity;
import android.widget.ArrayAdapter;

import facci.pm.ta3.sqlite.trabajoautonomo3sqlite.adapter.ShoppingItemAdapter;
import facci.pm.ta3.sqlite.trabajoautonomo3sqlite.database.helper.ShoppingElementHelper;
import facci.pm.ta3.sqlite.trabajoautonomo3sqlite.database.model.ShoppingItem;
import java.util.ArrayList;

public class ShoppingItemDB {

    private static final String TEXT_TYPE = " TEXT";
    private static final String COMMA_SEP = ",";

    private ShoppingElementHelper dbHelper;

    public ShoppingItemDB(Context context) {
        // Create new helper
        dbHelper = new ShoppingElementHelper(context);
    }

    /* Inner class that defines the table contents */
    public static abstract class ShoppingElementEntry implements BaseColumns {
        public static final String TABLE_NAME = "entry";
        public static final String COLUMN_NAME_TITLE = "title";

        public static final String CREATE_TABLE = "CREATE TABLE " +
                TABLE_NAME + " (" +
                _ID + " INTEGER PRIMARY KEY AUTOINCREMENT" + COMMA_SEP +
                COLUMN_NAME_TITLE + TEXT_TYPE + " )";

        public static final String DELETE_TABLE = "DROP TABLE IF EXISTS " + TABLE_NAME;
    }

    public void insertElement(String productName) {
        //TODO: Todo el código necesario para INSERTAR un Item a la Base de datos
        // Lo que tenemos que añadir es la siguiente linea=
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        // este metodo lo que haces es llamar a a "dbHelper" para que se puedan escribir datos
        // en la base de datos


        ContentValues values = new ContentValues();
        values.put(ShoppingElementEntry.COLUMN_NAME_TITLE, productName);

        long newRowId = db.insert(ShoppingElementEntry.TABLE_NAME, null, values);


    }

    public ArrayList<ShoppingItem> getAllItems() {
        //El get Readable lee los datos que contiene el array list en conjunto del Product
        SQLiteDatabase db = dbHelper.getReadableDatabase();

        ArrayList<ShoppingItem> shoppingItems = new ArrayList<>();


        String[] allColumns = {ShoppingElementEntry._ID,
                ShoppingElementEntry.COLUMN_NAME_TITLE};
        String selection = ShoppingElementEntry.COLUMN_NAME_TITLE + " = ?";
        String[] selectionArgs = {"My Title"};

        String sortOrder =
                ShoppingElementEntry.COLUMN_NAME_TITLE;

        Cursor cursor = dbHelper.getReadableDatabase().query(
                ShoppingElementEntry.TABLE_NAME,    // The table to query
                allColumns,                         // The columns to return
                null,                               // The columns for the WHERE clause
                null,                               // The values for the WHERE clause
                null,                               // don't group the rows
                null,                               // don't filter by row groups
                null                                // The sort order
        );

        cursor.moveToFirst();

        while (!cursor.isAfterLast()) {
            ShoppingItem shoppingItem = new ShoppingItem(getItemId(cursor), getItemName(cursor));
            shoppingItems.add(shoppingItem);
            cursor.moveToNext();
        }
        // make sure to close the cursor
        cursor.close();
        dbHelper.getReadableDatabase().close();
        return shoppingItems;
    }

    private long getItemId(Cursor cursor) {
        return cursor.getLong(cursor.getColumnIndexOrThrow(ShoppingElementEntry._ID));
    }

    private String getItemName(Cursor cursor) {
        return cursor.getString(cursor.getColumnIndexOrThrow(ShoppingElementEntry.COLUMN_NAME_TITLE));
    }

      //El codigo clear all items esta comentado por razones de captura de pantallas y
    //replicacion de los datos al momento de instalar y desinstalar la app
    public void clearAllItems() {
  //      SQLiteDatabase db = dbHelper.getWritableDatabase();

    //    //TODO: Todo el código necesario para ELIMINAR todos los Items de la Base de datos
      //  String Selection = ShoppingElementEntry.COLUMN_NAME_TITLE + "title";
        //String [] selectionArgs = {"My titll2"};
        //int delete = db.delete(ShoppingElementEntry.TABLE_NAME,Selection,selectionArgs);

    }


         //Codigo para actulizar la base de datos con la opcion del menu
    public void updateItem(ShoppingItem shoppingItem) {

        //TODO: Todo el código necesario para ACTUALIZAR un Item en la Base de datos
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        String title = "Title";
        ContentValues values =new ContentValues();
        values.put(ShoppingElementEntry.COLUMN_NAME_TITLE,title);
        String selection = ShoppingElementEntry.COLUMN_NAME_TITLE + "Like ?";
        String [] selectionArg = {"My old title"};
        int count = db.update(
                ShoppingElementEntry.TABLE_NAME,
                values,
                selection,
                selectionArg
        );







 //       )


    }



    public void deleteItem(ShoppingItem shoppingItem) {
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        //TODO: Todo el código necesario para ELIMINAR un Item de la Base de datos

        String Selection = ShoppingElementEntry.COLUMN_NAME_TITLE + "title";
        String [] selectionArgs = {"My title"};
        int delete = db.delete(ShoppingElementEntry.TABLE_NAME,Selection,selectionArgs);




    }
}
